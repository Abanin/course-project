﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Faculty
    {
        [Key]
        public int FacultyID { get; set; }
        public string FacultyName { get; set; }
        [NotMapped]
        public ObservableCollection<Speciality> FacultySpecialities { get; set; } = new ObservableCollection<Speciality>();
        public Faculty()
        {
                
        }
        public Faculty(string facultyName)
        {
            FacultyName = facultyName;
        }
    }
    
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Speciality
    {
        [Key]
        public int SpecialityID { get; set; }
        public string SpecialityName { get; set; }
        public int FacultyConnectionID { get; set; }
        public Speciality()
        { }
        public Speciality(string specialityName, int facultyConnectionID)
        {
            SpecialityName = specialityName;
            FacultyConnectionID = facultyConnectionID;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class ScheduleItem
    {
        [Key]
        public int SubjectID { get; set; }
        public int GroupConnectionID { get; set; }
        public string SubjectName { get; set; }
        public string TeacherName { get; set; }
        public string TypeOfSubject { get; set; }
        public string NumberOfAudience { get; set; }
        public int NumberOfDay { get; set; }
        public int NumberInTheSchedule { get; set; }
        public int NumberOfTheWeek { get; set; }
        public ScheduleItem()
        {

        }
        public ScheduleItem(string subjectName, string teacherName, string typeOfSubject, string numberOfAudience, int numberOfDay, int numberInTheSchedule, int numberOfTheWeek, int groupConnectionID)
        {
            SubjectName = subjectName;
            TeacherName = teacherName;
            TypeOfSubject = typeOfSubject;
            NumberOfAudience = numberOfAudience;
            NumberOfDay = numberOfDay;
            NumberInTheSchedule = numberInTheSchedule;
            NumberOfTheWeek = numberOfTheWeek;
            GroupConnectionID = groupConnectionID;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class PersonalGroup
    {
        [Key]
        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public string Faculty { get; set; }
        public string Speciality { get; set; }
        [NotMapped]
        public ObservableCollection<ScheduleItem> SubjectsForFirstWeek { get; set; } = new ObservableCollection<ScheduleItem>();
        [NotMapped]
        public ObservableCollection<ScheduleItem> SubjectsForSecondWeek { get; set; } = new ObservableCollection<ScheduleItem>();
        public PersonalGroup()
        {
        }
        public PersonalGroup(string faculty, string speciality, string groupName)
        {
            GroupName = groupName;
            Faculty = faculty;
            Speciality = speciality;
        }
        public void ShowSchedule()
        {

        }
    }
}

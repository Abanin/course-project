﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork.DatabaseForApplication
{
    public class DataForApplication : DbContext
    {
        public DataForApplication() : base("DefaultConnection")
        {
            Database.SetInitializer<DataForApplication>(null);
        }
        public DbSet<Faculty> Faculties { get; set; }
        public DbSet<Speciality> Specialities { get; set; }
        public DbSet<PersonalGroup> Groups { get; set; }
        public DbSet<ScheduleItem> ScheduleItems { get; set; }
    }
}
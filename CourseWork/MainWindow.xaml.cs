﻿using ClassLibrary;
using CourseWork.DatabaseForApplication;
using CourseWork.Windows;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace CourseWork
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    [AddINotifyPropertyChangedInterface]
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            database = new DataForApplication();
            AddFaculties();
            AddGroups();
            EditingFacultiesComboBox.ItemsSource = Faculties;
            EditingFacultiesComboBox.DisplayMemberPath = "FacultyName";
            AddGroupFacultiesComboBox.ItemsSource = Faculties;
            AddGroupFacultiesComboBox.DisplayMemberPath = "FacultyName";
            DayOfTheWeekComboBox.ItemsSource = DaysOfTheWeek;
            TimeOfTheSubjectComboBox.ItemsSource = TimeOfTheSubjects;
            AddSubjectGroupsComboBox.ItemsSource = Groups;
            AddSubjectGroupsComboBox.DisplayMemberPath = "GroupName";
            
        }
        public ObservableCollection<PersonalGroup> Groups { get; set; }
        public ObservableCollection<Faculty> Faculties { get; set; }
        public ObservableCollection<Speciality> Specialities { get; set; }
        public ObservableCollection<ScheduleItem> ScheduleItems { get; set; }
        public string[] DaysOfTheWeek = new string[] { "Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця" };
        public string[] TimeOfTheSubjects = new string[] { "8:30 - 9:50", "10:00 - 11:20", "11:40 - 13:00", "13:30 - 14:50", "15:00 - 16:20", "16:30 - 17:50" };
        public Faculty SelectedFacultyComboBox { get;
            set; }
        public Faculty SelectedFacultyListBox { get; set; }
        public DataForApplication database { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        private void ShowScheduleButton_Click(object sender, RoutedEventArgs e)
        {
            if (GroupsList.SelectedIndex < 0)
            {
                MessageBox.Show("Виберіть групу зі списку", "Помилка", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
            else
            {
                ShowSchedule(Groups[GroupsList.SelectedIndex]);
            }
        }
        public void ShowSchedule(PersonalGroup SelectedGroup)
        {
            ScheduleWindow scheduleWindow = new ScheduleWindow(SelectedGroup, database);
            scheduleWindow.Show();
        }

        private void SortFacultyDescButton_Click(object sender, RoutedEventArgs e)
        {
            GroupsList.Items.SortDescriptions.Clear();
            GroupsList.Items.SortDescriptions.Add(new SortDescription("Faculty", ListSortDirection.Descending));
            Groups = new ObservableCollection<PersonalGroup>(Groups.OrderByDescending(x => x.Faculty).ToList());
        }

        private void SortSpecialityDescButton_Click(object sender, RoutedEventArgs e)
        {
            GroupsList.Items.SortDescriptions.Clear();
            GroupsList.Items.SortDescriptions.Add(new SortDescription("Speciality", ListSortDirection.Descending));
            Groups = new ObservableCollection<PersonalGroup>(Groups.OrderByDescending(x => x.Speciality).ToList());
        }

        private void SortFacultyAscButton_Click(object sender, RoutedEventArgs e)
        {
            GroupsList.Items.SortDescriptions.Clear();
            GroupsList.Items.SortDescriptions.Add(new SortDescription("Faculty", ListSortDirection.Ascending));
            Groups = new ObservableCollection<PersonalGroup>(Groups.OrderBy(x => x.Faculty).ToList());
        }

        private void SortSpecialityAscButton_Click(object sender, RoutedEventArgs e)
        {
            GroupsList.Items.SortDescriptions.Clear();
            GroupsList.Items.SortDescriptions.Add(new SortDescription("Speciality", ListSortDirection.Ascending));
            Groups = new ObservableCollection<PersonalGroup>(Groups.OrderBy(x => x.Speciality).ToList());
        }
        public void AddFaculties()
        {
            database.Faculties.Load();
            database.Specialities.Load();
            Faculties = new ObservableCollection<Faculty>(database.Faculties.ToList());
            Specialities = new ObservableCollection<Speciality>(database.Specialities.ToList());
            for(int i = 0; i < Faculties.Count; i++)
            {
                for(int j = 0; j < Specialities.Count; j++)
                {
                    if(Faculties[i].FacultyID == Specialities[j].FacultyConnectionID)
                    {
                        Faculties[i].FacultySpecialities.Add(Specialities[j]);
                    }
                }
            }
        }
        public void AddGroups()
        {
            database.Groups.Load();
            database.ScheduleItems.Load();
            Groups = new ObservableCollection<PersonalGroup>(database.Groups.ToList());
            ScheduleItems = new ObservableCollection<ScheduleItem>(database.ScheduleItems.ToList());
            for(int i = 0; i < Groups.Count; i++)
            {
                for(int j = 0; j < ScheduleItems.Count; j++)
                {
                    if(Groups[i].GroupID == ScheduleItems[j].GroupConnectionID)
                    {
                        if(ScheduleItems[j].NumberOfTheWeek == 0)
                        {
                            Groups[i].SubjectsForFirstWeek.Add(ScheduleItems[j]);
                        }
                        else
                        {
                            Groups[i].SubjectsForSecondWeek.Add(ScheduleItems[j]);
                        }
                    }
                }
            }
        }
        private void EditFacultyButton_Click(object sender, RoutedEventArgs e)
        {
            Regex reg = new Regex(@"^(?:(?:[^Ьа-я]'?[а-яієїщґ]+[-']?)+)$");
            Match m;
            m = reg.Match(SpecialityNameFacultyTextBox.Text);
            if (EditingFacultiesComboBox.SelectedIndex < 0 || m.Success == false)
            {
                MessageBox.Show("Помилка при введенні даних", "Помилка додання спеціальності", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
            else
            {
                Speciality speciality = new Speciality(SpecialityNameFacultyTextBox.Text, Faculties[EditingFacultiesComboBox.SelectedIndex].FacultyID);
                Faculties[EditingFacultiesComboBox.SelectedIndex].FacultySpecialities.Add(speciality);
                database.Specialities.Attach(speciality);
                database.Entry(speciality).State = EntityState.Added;
                SaveChanges();
                EditingFacultiesComboBox.SelectedIndex = -1;
                SpecialityNameFacultyTextBox.Text = "";
            }
        }

        private void AddGroupButton_Click(object sender, RoutedEventArgs e)
        {
            Regex reg = new Regex(@"^(?:[А-ЯЇІЄ]+(?:[а-яїєі][А-ЯЇІЄ]?)*-\d+)$");
            Match m;
            m = reg.Match(AddGroupGroupTextBox.Text);
            if(AddGroupFacultiesComboBox.SelectedIndex < 0 || AddGroupSpecialitiesComboBox.SelectedIndex < 0 || m.Success == false)
            {
                MessageBox.Show("Помилка при введенні даних", "Помилка додання групи", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
            else
            {
                PersonalGroup personalGroup = new PersonalGroup(SelectedFacultyComboBox.FacultyName, SelectedFacultyComboBox.FacultySpecialities[AddGroupSpecialitiesComboBox.SelectedIndex].SpecialityName, AddGroupGroupTextBox.Text);
                Groups.Add(personalGroup);
                database.Groups.Attach(personalGroup);
                database.Entry(personalGroup).State = EntityState.Added;
                SaveChanges();
                AddGroupFacultiesComboBox.SelectedIndex = -1;
                AddGroupSpecialitiesComboBox.SelectedIndex = -1;
                AddGroupGroupTextBox.Text = "";
            }
        }

        private void AddSubjectButton_Click(object sender, RoutedEventArgs e)
        {
            Regex reg = new Regex(@"^(?:(?:[^Ьа-я]'?[а-яієїщґ]+[-' .]?)+)|(?:[А-ЯЇІЄ]+(?:[а-яїєі][А-ЯЇІЄ]?)*)$");
            Regex regTeacher = new Regex(@"^(?:(?:[^Ьа-я]'?[а-яієїщґ]*[-' .]{1,2})+)$");
            Match m;
            Match k;
            m = reg.Match(SubjectNameTextBox.Text);
            k = regTeacher.Match(TeacherNameTextBox.Text);
            if (AddSubjectGroupsComboBox.SelectedIndex < 0 || DayOfTheWeekComboBox.SelectedIndex < 0 || TimeOfTheSubjectComboBox.SelectedIndex < 0 || m.Success == false || k.Success == false || AreDigitsOnly(NumberOfAudience.Text) == false )
            {
                MessageBox.Show("Помилка при введенні даних", "Помилка додання предмету до розкладу", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
            else
            {
                string typeOfSubject;
                if(LectureRadioButton.IsChecked == true)
                {
                    typeOfSubject = LectureRadioButton.Content.ToString();
                }
                else if(PracticumRadioButton.IsChecked == true)
                {
                    typeOfSubject = PracticumRadioButton.Content.ToString();
                }
                else
                {
                    typeOfSubject = LaboratoryRadioButton.Content.ToString();
                }
                if(FirstWeekRadioButton.IsChecked == true)
                {
                    ScheduleItem scheduleItem = new ScheduleItem(SubjectNameTextBox.Text, TeacherNameTextBox.Text, typeOfSubject, "ауд. " + NumberOfAudience.Text, DayOfTheWeekComboBox.SelectedIndex, TimeOfTheSubjectComboBox.SelectedIndex, 0, Groups[AddSubjectGroupsComboBox.SelectedIndex].GroupID);
                    Groups[AddSubjectGroupsComboBox.SelectedIndex].SubjectsForFirstWeek.Add(scheduleItem);
                    database.ScheduleItems.Attach(scheduleItem);
                    database.Entry(scheduleItem).State = EntityState.Added;
                    SaveChanges();
                    AddSubjectGroupsComboBox.SelectedIndex = -1;
                    DayOfTheWeekComboBox.SelectedIndex = -1;
                    TimeOfTheSubjectComboBox.SelectedIndex = -1;
                    TeacherNameTextBox.Text = "";
                    NumberOfAudience.Text = "";
                    SubjectNameTextBox.Text = "";
                }
                else
                {
                    ScheduleItem scheduleItem = new ScheduleItem(SubjectNameTextBox.Text, TeacherNameTextBox.Text, typeOfSubject, "ауд. " + NumberOfAudience.Text, DayOfTheWeekComboBox.SelectedIndex, TimeOfTheSubjectComboBox.SelectedIndex, 1, Groups[AddSubjectGroupsComboBox.SelectedIndex].GroupID);
                    Groups[AddSubjectGroupsComboBox.SelectedIndex].SubjectsForSecondWeek.Add(scheduleItem);
                    database.ScheduleItems.Attach(scheduleItem);
                    database.Entry(scheduleItem).State = EntityState.Added;
                    SaveChanges();
                    AddSubjectGroupsComboBox.SelectedIndex = -1;
                    DayOfTheWeekComboBox.SelectedIndex = -1;
                    TimeOfTheSubjectComboBox.SelectedIndex = -1;
                    TeacherNameTextBox.Text = "";
                    NumberOfAudience.Text = "";
                    SubjectNameTextBox.Text = "";
                }
            }
        }
        public bool AreDigitsOnly(string textBoxString)
        {
            foreach (char c in textBoxString)
            {
                if (c < '0' || c > '9')
                    return false;
            }
            return true;
        }

        private void DeleteGroupButton_Click(object sender, RoutedEventArgs e)
        {
            if(GroupsListForDelete.SelectedItem == null)
            {
                MessageBox.Show("Оберіть групу зі списку", "Помилка видалення", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
            else
            {
                for(int i = 0; i < ScheduleItems.Count; i++)
                {
                    if(Groups[GroupsListForDelete.SelectedIndex].GroupID == ScheduleItems[i].GroupConnectionID)
                    {
                        database.Entry(ScheduleItems[i]).State = EntityState.Deleted;
                        database.ScheduleItems.Remove(ScheduleItems[i]);
                    }
                }
                database.Entry((PersonalGroup)GroupsListForDelete.SelectedItem).State = EntityState.Deleted;
                database.Groups.Remove((PersonalGroup)GroupsListForDelete.SelectedItem);
                SaveChanges();
                Groups.Remove((PersonalGroup)GroupsListForDelete.SelectedItem);
            }
        }

        private void DeleteFaculty_Click(object sender, RoutedEventArgs e)
        {
            if(FacultiesListForDelete.SelectedItem == null)
            {
                MessageBox.Show("Оберіть факультет зі списку", "Помилка видалення", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
            else
            {
                for(int i = 0; i < Specialities.Count; i++)
                {
                    if(Faculties[FacultiesListForDelete.SelectedIndex].FacultyID == Specialities[i].FacultyConnectionID)
                    {
                        database.Entry(Specialities[i]).State = EntityState.Deleted;
                        database.Specialities.Remove(Specialities[i]);
                    }
                }
                database.Entry((Faculty)FacultiesListForDelete.SelectedItem).State = EntityState.Deleted;
                database.Faculties.Remove((Faculty)FacultiesListForDelete.SelectedItem);
                SaveChanges();
                Faculties.Remove((Faculty)FacultiesListForDelete.SelectedItem);
            }
        }

        private void DeleteSpeciality_Click(object sender, RoutedEventArgs e)
        {
            if(SpecialitiesListForDelete.SelectedItem == null)
            {
                MessageBox.Show("Оберіть спеціальність зі списку", "Помилка видалення", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
            else
            {
                database.Entry((Speciality)SpecialitiesListForDelete.SelectedItem).State = EntityState.Deleted;
                database.Specialities.Remove((Speciality)SpecialitiesListForDelete.SelectedItem);
                SaveChanges();
                SelectedFacultyListBox.FacultySpecialities.Remove((Speciality)SpecialitiesListForDelete.SelectedItem);
            }
        }

        private void AddFacultyButton_Click(object sender, RoutedEventArgs e)
        {
            Regex reg = new Regex(@"^(?:(?:[^Ьа-я]'?[а-яієїщґ]+[-' .]?)+)$");
            Regex regFaculty = new Regex(@"^(?:[А-ЯЇІЄ]+(?:[а-яїєі][А-ЯЇІЄ]?)*)$");
            Match m;
            Match k;
            m = regFaculty.Match(EditingFacultiesComboBox.Text);
            k = reg.Match(SpecialityNameFacultyTextBox.Text);
            if(m.Success == false || k.Success == false || EditingFacultiesComboBox.SelectedItem != null)
            {
                MessageBox.Show("Помилка при введенні даних", "Помилка додання предмету до розкладу", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
            else
            {
                Faculty faculty = new Faculty(EditingFacultiesComboBox.Text);
                Faculties.Add(faculty);
                database.Faculties.Attach(faculty);
                database.Entry(faculty).State = EntityState.Added;
                SaveChanges();
                Speciality speciality = new Speciality(SpecialityNameFacultyTextBox.Text, Faculties[Faculties.Count - 1].FacultyID);
                Faculties[Faculties.Count - 1].FacultySpecialities.Add(speciality);          
                database.Specialities.Attach(speciality);
                database.Entry(speciality).State = EntityState.Added;
                SaveChanges();
                EditingFacultiesComboBox.Text = "";
                SpecialityNameFacultyTextBox.Text = "";
            }
        }
        public void SaveChanges()
        {
            bool saveFailed;
            do
            {
                saveFailed = false;
                try
                {
                    database.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;
                    ex.Entries.Single().Reload();
                }
            } while (saveFailed);
        }
    }
}

﻿using ClassLibrary;
using CourseWork.DatabaseForApplication;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CourseWork.Windows
{
    /// <summary>
    /// Логика взаимодействия для ScheduleWindow.xaml
    /// </summary>
    [AddINotifyPropertyChangedInterface]
    public partial class ScheduleWindow : Window, INotifyPropertyChanged
    {
        public ScheduleWindow(PersonalGroup group, DataForApplication database)
        {
            Group = group;
            DataContext = this;
            this.database = database;
            InitializeComponent();
        }
        public PersonalGroup Group { get; set; }
        public string AudienceText { get; set; }
        public DataForApplication database { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private void DeleteFirstWeekSubjectButton_Click(object sender, RoutedEventArgs e)
        {
            if(DeleteFirstWeekSubjectComboBox.SelectedItem == null)
            {
                MessageBox.Show("Оберіть предмет зі списку", "Помилка видалення", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
            else
            {
                database.Entry((ScheduleItem)DeleteFirstWeekSubjectComboBox.SelectedItem).State = EntityState.Deleted;
                database.ScheduleItems.Remove((ScheduleItem)DeleteFirstWeekSubjectComboBox.SelectedItem);
                SaveChanges();
                Group.SubjectsForFirstWeek.Remove((ScheduleItem)DeleteFirstWeekSubjectComboBox.SelectedItem);
            }
        }

        private void DeleteSecondWeekSubjectButton_Click(object sender, RoutedEventArgs e)
        {
            if (DeleteSecondWeekSubjectComboBox.SelectedItem == null)
            {
                MessageBox.Show("Оберіть предмет зі списку", "Помилка видалення", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
            else
            {
                database.Entry((ScheduleItem)DeleteSecondWeekSubjectComboBox.SelectedItem).State = EntityState.Deleted;
                database.ScheduleItems.Remove((ScheduleItem)DeleteSecondWeekSubjectComboBox.SelectedItem);
                SaveChanges();
                Group.SubjectsForSecondWeek.Remove((ScheduleItem)DeleteSecondWeekSubjectComboBox.SelectedItem);
            }
        }
        public void SaveChanges()
        {
            bool saveFailed;
            do
            {
                saveFailed = false;
                try
                {
                    database.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;
                    ex.Entries.Single().Reload();
                }
            } while (saveFailed);
        }
    }
}
